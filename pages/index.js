import Head from 'next/head'
import Header from '@components/Header'
import Footer from '@components/Footer'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Busybodyloaded.tk</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Header title="Welcome to my app!" />
        <p className="description">
         Site is  still under construction🚧 


  
<code>pages/index.js</code>
        </p>
      </main>

      <Footer />
    </div>
  )
}
